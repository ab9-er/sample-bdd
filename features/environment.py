from behave import *
from selenium import webdriver
from selenium.webdriver.firefox.options import Options


def before_scenario(context, scenario):
    if "frontend" in scenario.tags:
        options = Options()
        options.headless = True
        context.browser = webdriver.Firefox(firefox_options=options)
        context.browser.maximize_window()

def after_scenario(context, scenario):
    if "frontend" in scenario.tags:
        context.browser.close()
        context.browser.quit()
