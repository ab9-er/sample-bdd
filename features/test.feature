Feature: Testing front end and back end


@frontend
Scenario: Happy Path
    Given the interface to add politicians to a database
    When adding details of a politician
        |name|country|yob|position|source_url|risk_level|
        |John Smith|UK|1950-01-01|Senior Member|http://google.com|High|
    Then the details are saved with a confirmation


@frontend
Scenario: No name input saves the entry with placeholder value
    Given the interface to add politicians to a database
    When adding details of a politician
        |name|country|yob|position|source_url|risk_level|
        ||UK|1950-01-01|Senior Member|http://google.com|High|
    Then the details are not saved


@frontend
Scenario: No country input saves the entry with placeholder value
    Given the interface to add politicians to a database
    When adding details of a politician
        |name|country|yob|position|source_url|risk_level|
        |John Smith||1950-01-01|Senior Member|http://google.com|High|
    Then the details are not saved


@frontend
Scenario: No year of birth input saves the entry with placeholder value
    Given the interface to add politicians to a database
    When adding details of a politician
        |name|country|yob|position|source_url|risk_level|
        |John Smith|UK||Senior Member|http://google.com|High|
    Then the details are not saved


@frontend
Scenario: No position input saves the entry with placeholder value
    Given the interface to add politicians to a database
    When adding details of a politician
        |name|country|yob|position|source_url|risk_level|
        |John Smith|UK|1950/01/01||http://google.com|High|
    Then the details are not saved


@frontend
Scenario: No source url input saves the entry with placeholder value
    Given the interface to add politicians to a database
    When adding details of a politician
        |name|country|yob|position|source_url|risk_level|
        |John Smith|UK|1950/01/01|Senior Member||High|
    Then the details are not saved


@frontend
Scenario: Invalid URL format gives a warning
    Given the interface to add politicians to a database
    When adding a URL in an incorrect format
        |source_url|
        |tata|
    Then a warning is shown next to the URL input


@api
Scenario: Create a new entitiy
    Given a CRUD URL
    When adding data for a new politician
        |name|position|country|
        |John Smith|chairman|UK|
    Then the details for the politician is saved


@api
Scenario: Read list of last 5 politicians
    Given a CRUD URL
    When fetching data for all politicians
    Then the details for the last 5 politicians are returned


@api
Scenario: Read list of last 5 politicians
    Given a CRUD URL
    When fetching data for a politician by their id
        |id|
        |5bd83c8417b9b5000930a003|
    Then the details for the politician is returned
