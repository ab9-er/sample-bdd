#! /usr/bin/python
from requests import get, post
from behave import *
import uuid
from random import randint


def analyze_response(resp_data):
    assert "name" in list(resp_data.keys()), "Response does not contain name"
    
    # assert "country" in list(resp_data.keys()), "Response does not contain country"
    # assert type(resp_data.get("country")) == str, "Country is not of type string"
    
    assert "yob" in list(resp_data.keys()), "Response does not contain yob"
    assert type(resp_data.get("yob")) == int, "Yob is not of type integer"
    # assert resp_data.get("yob") > 0, "Yob is not greater than 0"
    
    assert "position" in list(resp_data.keys()), "Response does not contain position"
    assert type(resp_data.get("position")) == str, "Position is not of type string"
    
    assert "risk" in list(resp_data.keys()), "Response does not contain risk"
    assert type(resp_data.get("risk")) == int, "Risk is not of type integer"


@step("a CRUD URL")
def crud_url(context):
    context.crud_url = "http://ec2-34-251-162-89.eu-west-1.compute.amazonaws.com/peps"


@step("adding data for a new politician")
def create_data(context):
    json_data = {}
    for row in context.table:
        json_data["name"] = row.get("name") + str(uuid.uuid1())
        json_data["yob"] = randint(1900, 2000)
        json_data["risk"] = randint(1, 5)
        json_data["position"] = row.get("position")
        json_data["country"] = row.get("country")

    context.response = post(context.crud_url, json=json_data)


@step("the details for the politician is saved")
def saved_details(context):
    resp = context.response.json()

    assert context.response.status_code == 201, "Invalid status code"
    assert resp.get("message") == "Entity created successfully!", "Success message missing"
    assert "id" in resp.keys(), "Created id not returned"


@step("fetching data for all politicians")
def get_politicians(context):
    context.response = get(context.crud_url)


@step("the details for the last 5 politicians are returned")
def last_5_politician_data(context):
    resp = context.response.json()

    assert len(resp) <= 5, "More than 5 politicians' data is returned"
    assert type(resp) == list, "Data returned is not of type list"
    for res in resp:
        analyze_response(res)


@step("fetching data for a politician by their id")
def get_politician_by_id(context):
    for row in context.table:
        context.response = get(context.crud_url + "/{}".format(row.get("id")))


@step("the details for the politician is returned")
def retuend_data(context):
    resp = context.response.json()
    analyze_response(resp)
