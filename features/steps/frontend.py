#! /usr/bin/python
from behave import *
import uuid
import time


@step("the interface to add politicians to a database")
def open_url(context):
    context.browser.get("http://ec2-34-251-162-89.eu-west-1.compute.amazonaws.com/")


@step("adding details of a politician")
def enter_details(context):
    name_input = lambda: context.browser.find_element_by_name("fullName")
    country_input = lambda: context.browser.find_element_by_name("country")
    yob_input = lambda: context.browser.find_element_by_name("yob")
    position_input = lambda: context.browser.find_element_by_name("position")
    source_url_input = lambda: context.browser.find_element_by_name("url")
    risk_level_dropdown = lambda: context.browser.find_element_by_name("risk")
    options = lambda: context.browser.find_elements_by_tag_name("option")
    context.custom_data = {}

    for row in context.table:
        context.custom_data["politician_name"] = "" if row.get("name") == "" else row.get("name") + " " + str(uuid.uuid1())
        name_input().send_keys(context.custom_data.get("politician_name"))
        country_input().send_keys(row.get("country"))
        yob_input().send_keys(row.get("yob"))
        position_input().send_keys(row.get("position"))
        source_url_input().send_keys(row.get("source_url"))
        risk_level_dropdown().click()

        for option in options():
            if option.text == row.get("risk_level"):
                option.click()


@step("adding a URL in an incorrect format")
def enter_url(context):
    source_url_input = lambda: context.browser.find_element_by_name("url")

    for row in context.table:
        source_url_input().send_keys(row.get("source_url"))


@step("a warning is shown next to the URL input")
def url_warning(context):
    warning_message = context.browser.find_elements_by_class_name("invalid-feedback")
    
    assert len(warning_message) == 1, "URL format warning message is not shown"
    assert warning_message[0].text == "Invalid URL format.", "Incorrect warning message"

@step("the details are saved with a confirmation")
def save_details(context):
    save_btn = context.browser.find_element_by_class_name("btn")
    save_btn.click()

    timeout = 60
    while timeout:
        confirmation_modal = context.browser.find_elements_by_class_name("modal-content")
        if len(confirmation_modal) == 1:
            break
        else:
            time.sleep(5)
            timeout -= 5
            if timeout == 0:
                raise Exception("Confirmation modal not displayed")
    
    saved_name = context.browser.find_element_by_css_selector(".modal-body span").get_property("textContent")

    assert str(saved_name) == str(context.custom_data.get("politician_name")), "Saved name {} is not same as the one we entered {}".format(saved_name, context.custom_data.get("politician_name"))
    
    confirm_btn = context.browser.find_element_by_css_selector(".modal-footer .btn")
    confirm_btn.click()


@step("the details are not saved")
def save_details_failed(context):
    save_btn = context.browser.find_element_by_class_name("btn")
    save_btn.click()

    timeout = 60
    while timeout:
        confirmation_modal = context.browser.find_elements_by_class_name("modal-content")
        if len(confirmation_modal) == 1:
            raise Exception("Confirmation modal is displayed even when an input is not supplied")
        else:
            time.sleep(5)
            timeout -= 5
            if timeout == 0:
                return True
