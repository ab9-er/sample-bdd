# BDD Tester

The tests can be run in three ways

1. Locally using pip

    If both pipenv and docker aren't installed, we can make use of the `requirements.txt` and install the dependencies locally and run the test.

    ```
    make install-local run-local
    ```

2. Locally using pipenv

    If pipenv is installed then we can run the test as follows

    ```
    make install run
    ```

3. Using docker

    A new docker image can be built and run either using docker-compose or running the image directly

    Using compose
    ```
    make run-dev
    ```

    Using docker
    ```
    make test
    ```

Once the tests are run, the results are displayed in the terminal.

NB: To run the tests without docker, we would need to install firefox and geckodriver and make sure geckodriver is in the PATH.

You can run the following to install them on a macOS.

```
brew cask install firefox
brew install geckodriver
```
