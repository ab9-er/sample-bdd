clean:
	$(info =====  $@  =====)
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*.~' -exec rm -f {} +
	find . -name '*.log' -exec rm -rf {} +
	find . -name '__pycache__' -exec rm -rf {} +
	find . -name 'pylint.html' -exec rm -rf {} +
	find . -name 'pylint.json' -exec rm -rf {} +
	find . -name '*.tar.gz' -exec rm -rf {} +

compress:
	tar -cvzf sample-bdd.tar.gz .

build:
	$(info =====  $@  =====)
	docker build -t sample-bdd .

generate-requirements:
	$(info =====  $@  =====)
	pipenv lock -r > requirements.txt
	pipenv lock -r --dev > requirements-dev.txt

install:
	$(info =====  $@  =====)
	pipenv --python $(shell which python) install --dev

install-local:
	$(info =====  $@  =====)
	pip install -r requirements.txt

install-dev:
	$(info =====  $@  =====)
	pip install -r requirements-dev.txt

run:
	$(info =====  $@  =====)
	pipenv run behave

run-local:
	$(info =====  $@  =====)
	behave

run-dev:
	$(info =====  $@  =====)
	docker-compose run --name sample-bdd-runner --rm sample-bdd

test:
	$(info =====  $@  =====)
	docker run --name sample-bdd-runner --rm sample-bdd:latest

.PHONY: clean build install install-local install-dev run run-local run-dev test
