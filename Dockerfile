FROM python:slim

ENV PATH ${PATH}:/geckodriver

RUN apt-get update && \
    apt-get install -y wget \
                       bash \
                       firefox-esr && \
    pip install pipenv && \
    wget https://github.com/mozilla/geckodriver/releases/download/v0.24.0/geckodriver-v0.24.0-linux64.tar.gz && \
    tar -zvxf geckodriver-v0.24.0-linux64.tar.gz && \
    rm geckodriver-v0.24.0-linux64.tar.gz && \
    mv ./geckodriver /usr/local/bin/geckodriver && \
    chmod a+x /usr/local/bin/geckodriver

COPY ./features /tests/features
COPY ./Pipfile /tests/Pipfile
COPY ./Pipfile.lock /tests/Pipfile.lock

WORKDIR /tests

RUN pipenv install --system

ENTRYPOINT ["behave"]
